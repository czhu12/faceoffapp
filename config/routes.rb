FaceoffApp::Application.routes.draw do 
  get "participations/create"

  get "statistics/index"

  get "profile_pictures/create"

  scope 'api', defaults: {format: 'json'} do
    root :to=>'home#app'
    # Returns all faceoofs of current user, as well as meta data 
    # for the faceoff incl. photo urls and names. Also returns 
    # all outstanding photos and requests
    match 'faceoffs', to: 'faceoffs#index', via:[:get]
    # Post to create a faceoff, send user_id and topic
    # PAYLOAD: 
    # {
    #   faceoffs: [
    #     {
    #       user_id: 1, // Identifier for whether or not user exists?
    #       topic: {name: 'Selfie'}
    #     },
    #     {},
    #     {}
    #   ]
    # }
    match 'faceoffs', to: 'faceoffs#create', via:[:post]

    match 'faceoffs/:id', to: 'faceoffs#update', via:[:put]
    # Returns all photos and requests, with associated user data 
    # for specified Faceoff
    match 'faceoffs/:faceoff_id', to: 'faceoffs#show', via:[:get]
    # Post to submit a new request to the Faceoff
    # Must include faceoff_id for request and a topic name
    # {
    #   faceoff_id: 1,
    #   topic: {name:'Selfie'}
    # }
    match 'faceoffs/:faceoff_id/requests', to:'requests#create', via:[:post]

    # Post to upload a photo to a specified request
    match 'requests/:request_id/photos', to: 'photos#create', via:[:post]
    
    match 'request_groups/points', to: 'points#create_bulk', via:[:post]

    match 'requests/:id', to: 'requests#show', via:[:get]
    # Post to create a new topic
    match 'topics', to: 'topics#create', via:[:post]
    # Returns all topics as well as counters
    match 'topics', to: 'topics#index', via:[:get]
    # Registers a device with a registration id to send real time push 
    # notifications to
    match 'devices', to: 'devices#create', via:[:post]
    match 'devices', to: 'devices#update', via:[:put]

    match 'users/login', to: 'users#login', via:[:post]
    match 'users/facebook', to: 'users#facebook', via:[:post]
    
    match 'users', to: 'users#create', via:[:post]
    match 'users', to: 'users#index', via:[:get]
    match 'users', to: 'users#update', via:[:put]
    match 'points', to: 'points#index', via:[:get]

    match 'profile_pictures', to:'profile_pictures#create', via:[:post]

    match 'contacts', to:'users#contacts', via:[:post]

    match 'search', to: 'search#index', via:[:get]

    match 'statistics', to: 'statistics#index', via:[:get]

    match 'facebook/friends', to: 'facebook#friends', via:[:get]

    match 'unseen_notifications', to: 'faceoff_notifications#index_unseen_notifications', via:[:get]

    match 'faceoffs/:faceoff_id/unseen_notifications', to: 'faceoff_notifications#show_unseen_notifications', via:[:get]

    match 'faceoffs/:faceoff_id/unseen_notifications', to: 'faceoff_notifications#destroy_unseen_notifications', via:[:delete]

    match 'should_notify_notifications', to: 'faceoff_notifications#index_should_notify', via:[:get]

    match 'should_notify_notifications', to: 'faceoff_notifications#destroy_should_notify', via:[:delete]

    match 'crash_reports', to: 'crash_reports#create', via:[:post]

    match 'photos/:id/comments', to: 'comments#create', via:[:post]

    match 'photos/:id/comments', to: 'comments#index', via:[:get]

    match 'unseen_comments', to: 'comments#unseen_comments', via:[:get]

    match 'invites', to: 'invites#create', via:[:post]
    match 'invites', to: 'invites#index', via:[:get]


  end
end
