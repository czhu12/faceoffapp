require 'rails_helper'

Rspec.describe APIWrapper do
  before :all do
    @policy = FacebookPolicy.new
    @api = FacebookAPIMock.new
    @cache = RedisCacheMock.new
    @api_wrapper = APIWrapper.new(policy, cache, api)
  end

  describe "API Wrapper"
    it "should fetch the data when it is not in the cache and put it in the cache" do
      key = "123"
      @api_wrapper.profile key
      value = @cache.get key

      value.should equal("{name: 'chris'}")
    end

    class FacebookAPIMock
      def profile
        return "{name: 'chris'}"
      end
    end

    class RedisCacheMock
      def initialize
        @cache = {}
      end

      def set(key, value)
        @cache[key] = value
      end

      def get
        @cache[key]
      end

      def exists
        not @cache[key].nil?
      end
    end
  end
end
