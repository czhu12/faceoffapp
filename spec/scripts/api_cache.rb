ENV['RAILS_ENV'] = ARGV[0] || 'production'
DIR = File.dirname(__FILE__) 
require DIR + '/../../config/environment'

Dir["#{Rails.root.to_s}/lib/caching/*.rb"].each {|file| require file }

class FacebookAPIMock
  # pretend to be getting from network
  def profile
    return "{name: 'chris'}"
  end
end

class RedisCacheMock
  def initialize
    @cache = {}
  end

  def set(key, value, options=nil)
    @cache[key] = value
  end

  def get(key, options=nil)
    @cache[key]
  end

  def exists(key)
    not @cache[key].nil?
  end
end

@policy = FacebookPolicy.new

@api = FacebookAPIMock.new
@cache = RedisCacheMock.new
@api_wrapper = APIWrapper.new(@policy, @cache, @api)

def run_test
  key = "123"
  @api_wrapper.profile key
  value = @cache.get key
  puts value

  #puts value == "{name: 'chris'}"
end

run_test()
