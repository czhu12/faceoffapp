# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#

#user1 = User.find_by_email("test@example.com")
#user2 = User.find_by_email("test2@example.com")
#user1.destroy unless user1 == nil
#user2.destroy unless user2 == nil
#
#user1 = User.new
#user1.email = "test@example.com"
#user1.password = "password"
#user1.password_confirmation = "password"
#user1.save
#
#user2 = User.new
#user2.email = "test2@example.com"
#user2.password = "password"
#user2.password_confirmation = "password"
#user2.save
#
#faceoff = Faceoff.new
#faceoff.save
#
#p1 = Participation.new
#p2 = Participation.new
#
#p1.user_id = user1.id
#p1.faceoff_id = faceoff.id
#
#p2.user_id = user1.id
#p2.faceoff_id = faceoff.id
#
#p1.save
#p2.save
#
#topic = Topic.find_or_create_by_name("YOMAMA")
#
#topic2 = Topic.find_or_create_by_name("Flappybird")
#
## user 1 makes request to user 2 that is not responded to
#Request.skip_callback(:create)
#request = Request.new
#request.user_id = user2.id
#request.participation_id = p1.id
#request.topic_id = topic.id
#request.save
## this will save it without creating notifications
#
## user 2 makes request to user 1 that is responded to
#photo = Photo.new
#photo.save(:validate => false)
#
#request2 = Request.new
#request2.user_id = user1.id
#request2.participation_id = p2.id
#request2.topic_id = topic2.id
#request2.photo_id = photo.id
## this will save it without creating notifications
#request2.save 
#
#Request.set_callback(:create)
