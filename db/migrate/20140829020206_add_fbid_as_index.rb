class AddFbidAsIndex < ActiveRecord::Migration
  def up
    add_index :users, :fbid
  end

  def down
    remove_index :users, :fbid
  end
end
