class AddIndexToUsers < ActiveRecord::Migration
  def change
    add_index :users, :email
    add_index :users, :access_token
  end
end
