class AddFbAccessTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fb_access_token, :string
    add_index :users, :fb_access_token
  end
end
