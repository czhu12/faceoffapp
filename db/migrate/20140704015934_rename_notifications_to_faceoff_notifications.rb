class RenameNotificationsToFaceoffNotifications < ActiveRecord::Migration
  def change
    rename_table :notifications, :faceoff_notifications
  end
end
