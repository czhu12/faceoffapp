class AddShouldNotifyToFaceoffNotifications < ActiveRecord::Migration
  def change
    add_column :faceoff_notifications, :should_notify, :boolean, :default => true
  end
end
