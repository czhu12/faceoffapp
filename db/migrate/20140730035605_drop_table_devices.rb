class DropTableDevices < ActiveRecord::Migration
  def up
    drop_table :devices
  end

  def down
  end
end
