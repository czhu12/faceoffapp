class RemoveGuestFromUsersTable < ActiveRecord::Migration
  def up
    remove_column :users, :guest
  end

  def down
  end
end
