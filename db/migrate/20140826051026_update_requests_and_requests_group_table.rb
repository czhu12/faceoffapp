class UpdateRequestsAndRequestsGroupTable < ActiveRecord::Migration
  def up
    remove_column :requests, :participation_id
    remove_column :requests, :topic_id
    add_column :request_groups, :topic_id, :integer
  end

  def down
    add_column :requests, :participation_id, :integer
    add_column :requests, :topic_id, :integer
    remove_column :request_groups, :topic_id
  end
end
