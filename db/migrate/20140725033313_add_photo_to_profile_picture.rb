class AddPhotoToProfilePicture < ActiveRecord::Migration
  def up
    add_attachment :profile_pictures, :photo
  end

  def down
    remove_attachment :profile_pictures, :photo
  end
end
