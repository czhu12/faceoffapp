class ChangeDefaultGuestValueToFalse < ActiveRecord::Migration
  def up
    change_column :users, :is_guest, :boolean, default: false
  end

  def down
    change_column :users, :is_guest, :boolean, default: true
  end
end
