class CreateRequestGroups < ActiveRecord::Migration
  def change
    create_table :request_groups do |t|
      t.integer :participation_id
      
      t.timestamps
    end
  end
end
