class AddFbidToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fbid, :integer, :limit => 8
  end
end
