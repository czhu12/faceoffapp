class AddNameToFaceoffs < ActiveRecord::Migration
  def change
    add_column :faceoffs, :name, :string
  end
end
