class AddCounterToTopic < ActiveRecord::Migration
  def change
    add_column :topics, :count, :integer
  end
end
