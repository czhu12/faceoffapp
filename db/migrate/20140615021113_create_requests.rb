class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :topic_id
      t.integer :participation_id
      t.integer :photo_id

      t.timestamps
    end
  end
end
