class AddFaceToPhotos < ActiveRecord::Migration
  def up
    add_attachment :photos, :face
  end
  def down
    remove_attachment :photos, :face
  end
end
