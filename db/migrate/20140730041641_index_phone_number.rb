class IndexPhoneNumber < ActiveRecord::Migration
  def up
    add_index :devices, :phone_number
  end

  def down
    remove_index :devices, :phone_number
  end
end
