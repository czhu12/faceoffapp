class AddSeenToNotifications < ActiveRecord::Migration
  def change
    add_column :faceoff_notifications, :seen, :boolean, :default => false
  end
end
