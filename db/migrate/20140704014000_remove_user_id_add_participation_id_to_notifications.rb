class RemoveUserIdAddParticipationIdToNotifications < ActiveRecord::Migration
  def up
    remove_column :notifications, :user_id
    add_column :notifications, :participation_id, :integer
  end

  def down
    add_column :notifications, :user_id, :integer
    remove_column :notifications, :participation_id
  end
end
