class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.integer :value
      t.boolean :seen, :default=>false
      t.integer :user_id
      t.integer :request_group_id

      t.timestamps
    end
  end
end
