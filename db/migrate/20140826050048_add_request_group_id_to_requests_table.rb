class AddRequestGroupIdToRequestsTable < ActiveRecord::Migration
  def change
    add_column :requests, :request_group_id, :integer
  end
end
