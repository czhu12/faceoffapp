class CreateCrashReports < ActiveRecord::Migration
  def change
    create_table :crash_reports do |t|
      t.string :filename
      t.text :stacktrace

      t.timestamps
    end
  end
end
