class CreateTableDevices < ActiveRecord::Migration
  def up
    create_table :devices do |t|
      t.integer :user_id
      t.string :phone_number
      t.timestamps
    end
  end

  def down
    drop_table :devices
  end
end
