class PointsNotifier  
  include Sidekiq::Worker

  def perform(value, sender_id, request_group_id)
    Point.create(
      value: value, 
      user_id: sender_id, 
      request_group_id: request_group_id
    )
  end
end
