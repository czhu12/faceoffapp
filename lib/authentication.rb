module Authentication
  def self.included(controller)
    controller.send :helper_method, :current_user, :logged_in?
    #controller.filter_parameter_logging :password, :password_confirmation
  end

  def current_user
    @current_user ||= User.find_by_access_token(params[:access_token]) if params[:access_token]
  end

  def logged_in?
    current_user
  end

  def login_required
    unless logged_in?
      render :json => {:error =>404}
    end
  end
end
