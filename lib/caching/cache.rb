require 'json'
require 'redis'

class RedisCache
  def initialize
    @redis = Redis.new
  end

  def set(key, value, options = nil)
    before_callback = options ? options[:before_callback] : nil
    after_callback = options ? options[:after_callback] : nil

    before_callback.call(@redis, key, value) unless before_callback.nil?
    @redis.set key, value.to_json
    after_callback.call(@redis, key, value) unless before_callback.nil?
  end
  
  def get(key, options = nil)
    before_callback = options ? options[:before_callback] : nil
    after_callback = options ? options[:after_callback] : nil

    before_callback.call(@redis, key) unless before_callback.nil?
    value = JSON.parse(@redis.get key)
    after_callback.call(@redis, key) unless before_callback.nil?

    return value
  end

  def exists(key)
    @redis.exists key
  end

  def clear_cache
  end
end
