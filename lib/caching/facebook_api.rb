require 'json'

class FacebookAPI
  include HTTParty
  base_uri 'https://graph.facebook.com'

  def initialize(fbid, access_token)
    @fbid = fbid
    @access_token = access_token
  end

  # Get a list of friends of a certain user with profile pictures.
  def friends
    response = self.class.get("/v2.1/#{@fbid}/friends", {query: {access_token: @access_token}})
    Rails.logger.debug(JSON.parse(response.body))
    if is_valid(response)
      Rails.logger.debug(JSON.parse(response.body))
      return JSON.parse(response.body)['data']
    end

    return nil
  end

  # Get the user information. Profile picture, name etc.
  def profile
    response = self.class.get(
      "/v2.1/#{@fbid}", {query: {access_token: @access_token, fields: 'picture, first_name, last_name, id'}}
    )

    # Handle erronous responses.
    if is_valid(response)
      return JSON.parse response.body
    end

    return nil
  end

  def profile_picture
    response = self.class.get(
      "/v2.1/#{@fbid}/picture", {query: {access_token: @access_token, type: 'large', redirect: 'false'}}
    )

    if is_valid(response)
      return JSON.parse(response.body)["data"]
    end

    return nil
  end
  private 
  def is_valid(response)
    Rails.logger.debug("======================")
    Rails.logger.debug(response)
    response.response.code == '200'
  end
end
