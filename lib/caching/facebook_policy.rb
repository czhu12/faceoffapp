class FacebookPolicy
  @expire_time = 20.minutes
  def initialize
  end

  # At any given time there are a few possible things that could happen:
  # cache hit: return value OR return & populate async OR populate sync
  # cache miss: return default value OR return default & populate async OR populate sync
  def fetch(cache, key, &get_method)
    if cache.exists key 
      return cache.get key
    else 
      value = get_method.call()
      cache.set key, value, {:after_callback => lambda { |redis, key, value| redis.expire key, expire_time}}
      return value
    end
  end

  # When new data comes in, we add it to the cache but then we can do a few things:
  # Set an expiration date.
  # Cache methods take a before callback and after callback that we can hook into doing stuff.
end
