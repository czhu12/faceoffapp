class APIWrapper
  def initialize(policy, cache, api)
    @policy = policy
    @cache = cache
    @api = api
  end

  # pass it a lambda get_method as a way to figure out how to get the data that you want cached.
  # The key for this might for instance be the users fbid
  def fetch(key, &get_value)
    return @policy.fetch @cache, key, &get_value
  end

  def friends(fbid)
    key = fbid.to_s + '-friends'
    lam = lambda { return @api.friends }
    fetch key, &lam
  end

  def profile(fbid)
    key = fbid.to_s + '-profile'
    lam = lambda { return @api.profile }
    return fetch key, &lam
  end

  def profile_picture(fbid)
    key = fbid.to_s + '-profile_picture'
    lam = lambda {return @api.profile_picture}
    return fetch(key, &lam)["url"]
  end
end
