require 'notifications/notification_manager'

class CommentsNotifier  
  include Sidekiq::Worker

  def perform(receiver_id, body)
    manager = Notification::NotificationManager.new
    message = get_message(receiver_id, body)
    manager.send(message, {:action=>"com.faceoff.app.COMMENT"})
  end

  def get_message(receiver_id, body) 
    user = User.find(receiver_id)
    return {
      user_id: receiver_id,
      user: user,
      body: body
    }
  end
end
