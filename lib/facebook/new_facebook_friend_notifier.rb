require 'notifications/notification_manager'

class NewFacebookFriendNotifier
  include Sidekiq::Worker
  def perform(new_facebook_user_id)
    # The algorithm could be as follows:
    # First, we get the current user
    # Next, we find all the facebook friends of this user.
    # Then in a loop, we notify all the facebook friends 
    # that the current user has joined the service.

    # 1. Get the current user
    new_facebook_user = User.find(new_facebook_user_id)
    facebook_friends = new_facebook_user.facebook_friends()
    manager = Notification::NotificationManager.new

    facebook_friends.each do |friend|
      message = get_message(friend.id, new_facebook_user)
      manager.send(message, {:action=>"com.faceoff.app.NEW_FACEBOOK_FRIEND"})
    end
  end

  def get_message(user_id, new_facebook_user)
    return {
      user_id: user_id,
      new_facebook_user: new_facebook_user
    }
  end
end
