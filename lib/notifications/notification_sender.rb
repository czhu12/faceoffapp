class NotificationSender  
  include Sidekiq::Worker

  def perform(request_id, participation_id)
    notification = FaceoffNotification.new
    notification.request_id = request_id
    notification.participation_id = participation_id
    notification.save
  end
end
