require 'notifications/notification_service.rb'

module Notification
  class NotificationManager
    def initialize
      @service = NotificationService.new
    end

    def send(message, options={})
      channel = get_user_channel(message[:user_id])
      @service.send(message, channel, options)
    end

    def get_user_channel(user_id)
      if Rails.env.development?
        user_channel = "user_" + user_id.to_s + "_#{Rails.env}"
      else 
        user_channel = "user_" + user_id.to_s
      end

      puts "NOTIFICATION SENT TO USER WITH CHANNEL OF #{user_channel}"
      return user_channel
    end
  end
end
