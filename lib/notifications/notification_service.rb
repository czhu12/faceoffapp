require 'yaml'
require 'net/http'
require 'uri'
require 'json'
require 'httparty'

class NotificationService
  def initialize
    @config = load_config
    @service = ParseNotificationService.new(@config)
  end

  def send(message, channel, options)
    @service.send(message, channel, options)
  end

  def load_config
    config = {}
    config = YAML.load_file('config/notification.yml')['notification_service']['parse_service']
    config
  end
end

class ParseNotificationService
  # Parse utilizes a Publisher-Subscriber pattern whereby we push to a certain 
  # channel identified by a unique string and then everyone who is subscribed 
  # to that channel will recieve the message. The channel will be identified by
  def initialize(config)
    #@config = config
  end

  def send(message, channel, options)
    puts message.inspect
    puts channel.inspect
    puts "Action: #{options[:action]}"
    action_type = options[:action] || "com.faceoff.app.NOTIFICATION"

    data = {
      "action"=> "com.faceoff.app.UPDATE_STATUS", 
      "type" => "response",
      "action_type" => action_type
    }.merge(message)

    body = {
      :channels => [
        channel
      ],
      :data => data
    }
    puts body.to_json
    @result = HTTParty.post("https://api.parse.com/1/push", 
    :body =>  body.to_json,
    :headers => { 'X-Parse-Application-Id' => 'QMDVzwxKoDfsD7aXUSxK1AVKbCtVURcZt0FZD9Su',
                  'X-Parse-REST-API-Key' => 'r3f61iB6liDbo3CKG4eWVPJFuaz2L0063q1lXY4T',
                  'Content-Type' => 'application/json' } )
    puts @result
  end
end
