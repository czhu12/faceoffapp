# == Schema Information
#
# Table name: faceoffs
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Faceoff < ActiveRecord::Base
  has_many :participations, :dependent => :destroy
  has_many :users, :through => :participations
  has_many :devices, :through => :users
  has_many :profile_pictures, :through => :users
  has_many :request_groups, :through => :participations
  has_many :requests, :through => :request_groups
  has_many :topics, :through => :requests
  has_many :faceoff_notifications, :through=>:requests
  has_many :photos, :through => :requests

  def get_participants
    self.users.map { |u| u.full_name }
  end

  def pending_requests(user_id)
    request_notifications_for(user_id)
  end

  def pending_responses(user_id)
    response_notifications_for(user_id)
  end

  def clear_notifications_for_user(user_id)
    request_notifications = request_notifications_for(user_id)
    request_notifications.update_all("seen = 't'") unless 
      (request_notifications.nil? || request_notifications.empty?)

    response_notifications = response_notifications_for(user_id)
    response_notifications.update_all("seen = 't'") unless
      (response_notifications.nil? || response_notifications.empty?)
  end

  def request_notifications_for(user_id)
    request_notifications = FaceoffNotification.joins(:participation, :request_group, :request)
      .where("faceoff_id=?", self.id)
      .where("requests.user_id=?", user_id)
      .where("faceoff_notifications.seen=?", false)
      .where("requests.photo_id IS NULL")

    request_notifications 
  end

  def response_notifications_for(user_id)

    response_notifications = FaceoffNotification.joins(:participation, :request_group, :request)
      .where("faceoff_id=?", self.id)
      .where("participations.user_id=?", user_id)
      .where("faceoff_notifications.seen=?", false)
      .where("requests.photo_id IS NOT NULL")

    response_notifications
  end

  def has_user?(user)
    self.users.include? user
  end

  def self.faceoff_with_users(user_ids)
    p = Participation.find_by_sql(["
      SELECT faceoff_id 
      FROM participations 
      WHERE participations.user_id IN (?)
      GROUP BY participations.faceoff_id 
      HAVING COUNT(*)=? 

      EXCEPT 
      SELECT faceoff_id 
      FROM participations 
      WHERE participations.user_id NOT IN (?)", 
        user_ids, 
        user_ids.count, 
        user_ids])

      if p and p.first
        p.first.faceoff
      else
        nil
      end
      
  end

  def most_recent_updated_request
    self.requests.last
  end

  def clear_comments_for(user)
    # TODO: This clears all the comments
    # Fuck it, lets just write this query
    sql = "
      UPDATE comments
      SET seen='true'
      WHERE comments.id IN 
      (
        SELECT comments.id FROM comments 
        INNER JOIN users
        ON comments.user_id = users.id
        INNER JOIN participations
        ON participations.user_id = users.id
        INNER JOIN faceoffs
        ON participations.faceoff_id = faceoffs.id
        WHERE users.id = ? AND faceoffs.id = ?
      )
    "
    return Comment.find_by_sql([sql, user.id, self.id])
  end
end
