class Comment < ActiveRecord::Base
  attr_accessible :body, :photo_id, :seen
  belongs_to :user
  belongs_to :photo
  has_one :receiver, :through => :photo, :source => :owner
  has_one :topic, :through => :photo

  validates :body, presence: true, allow_blank: false
  validates_presence_of :user_id
  validates_presence_of :photo_id

end
