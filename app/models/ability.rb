class Ability
  include CanCan::Ability

  def initialize(user)
    # A user can create a request group if
    # the user is in the faceoff
    can :create, RequestGroup do |group|
      group.faceoff.has_user? user
    end

    can [:read, :update], Faceoff do |faceoff|
      faceoff.has_user? user
    end

    can [:update], User do |u|
      user == u
    end
  end
end
