class CrashReport < ActiveRecord::Base
  attr_accessible :filename, :stacktrace
end
