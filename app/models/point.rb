require 'notifications/notification_manager'

class Point < ActiveRecord::Base
  attr_accessible :request_group_id, :seen, :user_id, :value

  belongs_to :request_group
  has_one :participation, :through => :request_group
  has_one :faceoff, :through => :participation
  belongs_to :user

  after_create :send_notification

  def send_notification
    faceoff_users = all_users
    faceoff_users.each do |user|

      manager = Notification::NotificationManager.new
      message = get_message(user)
      manager.send(message, {:action=>"com.faceoff.app.FEEDBACK"})
    end
  end

  def get_message(receiver)
    sender = self.user
    message = {
      user_id: receiver.id,
      value: self.value
    }
  end

  def all_users
    self.faceoff.users
  end
end
