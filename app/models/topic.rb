# == Schema Information
#
# Table name: topics
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  count      :integer
#  name       :string(255)
#

class Topic < ActiveRecord::Base
  attr_accessible :name
  validates_uniqueness_of :name
  belongs_to :user

  before_create :set_count

  def set_count
    self.count = 0;
  end

  def inc
    if self.update_attribute(:count, self.count + 1)
        return self
    else
      return { error: topic.errors }
    end
  end

  def self.create_or_fetch_if_exists(name)
    topic = Topic.find_by_name(name)
    if not topic
      topic = Topic.new(name)
      if topic.save
        return topic
      else
        return false
      end
    end

    return topic
  end
end
