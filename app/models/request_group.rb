class RequestGroup < ActiveRecord::Base
  paginates_per 5

  attr_accessible :participation_id
  
  has_many :requests
  has_many :photos, :through => :requests

  belongs_to :participation
  has_one :faceoff, :through => :participation

  belongs_to :topic
  has_one :sender, :through => :participation, :source => :user
  has_many :users, :through => :requests, :source => :user

  validates_presence_of :participation_id
  validates_presence_of :topic_id

  after_save :touch_faceoff
  before_create :increment_topic

  def sender
    self.participation.user
  end

  def increment_topic
    self.topic.inc
  end

  def to_sql_values(request_group_config)
    values_array = []
    request_group_config.each do |config|
      get_function = config[:get_value]
      values_array.push(get_function.call(self))
    end

    values_comma = values_array * ","
    "(#{values_comma})"
  end

  def touch_faceoff
    self.faceoff.touch
  end
end
