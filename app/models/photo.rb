require 'notifications/notification_sender'
# == Schema Information
#
# Table name: photos
#
#  id                :integer          not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  face_file_name    :string(255)
#  face_content_type :string(255)
#  face_file_size    :integer
#  face_updated_at   :datetime
#

class Photo < ActiveRecord::Base
  attr_accessible :face
  has_attached_file :face, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :face, :content_type => /\Aimage\/.*\Z/
  validates_attachment_presence :face
  after_commit :create_notification, :on => :create
  before_save :touch_request_group

  has_one :request
  has_one :request_group, :through => :request
  has_one :topic, :through => :request_group
  has_one :owner, :through => :request, :source => :user

  has_many :comments

  def create_notification
    NotificationSender.perform_async(
      self.request.id,
      self.request.request_group.participation.id
    )
  end

  def touch_request_group
    self.request.touch
    self.request.request_group.touch
  end

  def give_points(add_points)
    self.points = self.points + add_points
    save
  end
end
