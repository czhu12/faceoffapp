# == Schema Information
#
# Table name: faceoff_notifications
#
#  id               :integer          not null, primary key
#  request_id       :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  participation_id :integer
#

require 'notifications/notification_manager'

class FaceoffNotification < ActiveRecord::Base
  belongs_to :request  
  belongs_to :participation
  has_one :photo, :through => :request
  has_one :faceoff, :through => :participation
  has_one :request_group, :through => :request
  has_one :topic, :through => :request_group

  after_commit :send_notification, :on => :create

  def is_request
    self.request.photo_id.nil?
  end

  def is_response
    not self.request.photo_id.nil?
  end

  def type
    if is_request
      return 'request'
    end
    return 'response'
  end

  def get_message
    message = {
      type: self.type,
    }

    # A notification has a request as a response
    # When a request is sent, the user the notification should be sent to is
    # the user that the request is pointing to, self.request.user
    # Sender   : self.participation.user
    # Receiver : self.request.user
    #
    # If a response is sent, the user the notification should be sent to is
    # the user that the participation is pointing to (as it is he who originally
    # sent the request). self.participation.user.id
    # Sender   : self.request.user
    # Receiver : self.participation.user

    if is_response
      message = message.merge({url: "http://#{FaceoffApp::Application::HOST_IP}" + self.request.photo.face.url})
      sender = self.request.user
      receiver = self.participation.user

      user = self.participation.user
      message = message.merge({ 
        topic: self.request.topic, 
        faceoff_id: self.participation.faceoff_id, 
        request_id: self.request.id, 
        sender: sender,
        user_id: receiver.id
      })
    end

    if is_request
      sender = self.participation.user
      receiver = self.request.user
      puts '================='
      puts sender.first_name + " is sending to " + receiver.first_name
      puts '================='

      message = message.merge({ 
        topic: self.request.topic,
        faceoff_id: self.participation.faceoff_id, 
        request_id: self.request.id, 
        sender: sender,
        user_id: receiver.id
      })
    end

    message
  end

  def send_notification
    manager = Notification::NotificationManager.new
    manager.send(get_message)
  end
end
