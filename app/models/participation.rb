# == Schema Information
#
# Table name: participations
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  faceoff_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Participation < ActiveRecord::Base
  attr_accessible :faceoff_id, :user_id
  belongs_to :user
  belongs_to :faceoff, touch: true
  
  has_many :request_groups

  validates_presence_of :user_id
  validates_presence_of :faceoff_id
  validates_uniqueness_of :user_id, :scope=>:faceoff_id
end
