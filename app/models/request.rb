# == Schema Information
#
# Table name: requests
#
#  id               :integer          not null, primary key
#  topic_id         :integer
#  participation_id :integer
#  photo_id         :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#

class Request < ActiveRecord::Base
  attr_accessible :face_id, :participation_id

  belongs_to :request_group, touch: true
  belongs_to :photo
  belongs_to :user
  
  has_many :faceoff_notifications

  validates_presence_of :request_group_id
  validates_presence_of :user_id
  
  def has_photo
    not self.photo_id.nil?
  end

  def topic 
    self.request_group.topic
  end

  def requester
    participation = Participation
      .joins(request_groups: :requests)
      .where("requests.id=?", self.id).first
    
    if participation.nil?
      return nil
    end

    User.find(participation.user_id)
  end

  def responder
    self.user
  end
end
