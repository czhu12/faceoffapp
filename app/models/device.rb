# == Schema Information
#
# Table name: devices
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  platform   :string(255)
#  enabled    :boolean
#  token      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Device < ActiveRecord::Base
  attr_accessible :phone_number
  belongs_to :user
  validates :phone_number, uniqueness: true
  validates :user_id, uniqueness: true
end
