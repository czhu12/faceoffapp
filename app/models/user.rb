# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string(255)
#  password_digest :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  access_token    :string(255)
#  guest           :boolean          default(TRUE)
#  first_name      :string(255)
#  last_name       :string(255)
#
Dir["#{Rails.root.to_s}/lib/caching/*.rb"].each {|file| require file }

class User < ActiveRecord::Base
  before_create :generateAccessToken
  has_secure_password
  attr_accessible :email, :password, :password_confirmation, :first_name, :last_name, :phone_number, :fbid, :fb_access_token
  validates_uniqueness_of :email, :message=>"email_taken"

  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create }

  has_many :participations, :dependent => :destroy
  has_many :faceoffs, :through => :participations
  has_many :requests, :dependent => :destroy # this is deceptive, it is actually requests that point at this user
  has_many :photos, :through => :requests
  has_many :topics

  has_one :profile_picture
  has_one :device

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def generateAccessToken
    begin 
      self.access_token = SecureRandom.hex
    end while self.class.exists?(access_token: access_token)
  end

  def find_faceoff_with(other_user_ids)
    other_users = User.includes(:faceoffs).find(other_user_ids)
    
    init = self.faceoffs
    intersection = other_users.reduce(init) do |intersect, user|
      intersect & user.faceoffs
    end
    
    return intersection.first
  end

  def participation_for_faceoff(faceoff_id)
    self.participations.where('faceoff_id=?', faceoff_id).first
  end

  def requests_count
    Request.joins(request_group: :participation)
    .where('participations.user_id=?', self.id)
    .count
  end

  def responses_count
    self.requests.where("photo_id is not null").count
  end
  
  def is_facebook_user
    not self.fbid.nil?
  end

  def profile_picture_url
    if is_facebook_user
      api_wrapper = APIWrapper.new(
        FacebookPolicy.new, 
        RedisCache.new, 
        FacebookAPI.new(self.fbid, self.fb_access_token)
      )

      return api_wrapper.profile_picture(self.fbid)
    end

    if not self.profile_picture.nil?
      return "http://#{FaceoffApp::Application::HOST_IP}#{self.profile_picture.photo.url}"
    else
      hashed_id = (self.id % 5) + 1
      return "http://#{FaceoffApp::Application::HOST_IP}/images/missing_profile_pictures/profile_picture_#{hashed_id}.jpg"
    end
  end

  def facebook_friends(fbid = self.fbid, token = self.fb_access_token)
    f = Facebook::FacebookAPI.new(fbid, token)
    fbid_array = []

    f.friends.each do |friend|
      fbid = friend['id']
      fbid_array.push(fbid)
    end

    User.find_all_by_fbid(fbid_array)
  end

  # Override to_json
  def to_json(options={})
    options[:except] ||= [:fb_access_token, :access_token]
    super(options)
  end

  def unseen_comments
    unseen_comments = Comment
      .where('seen=?', false)
      .where('user_id=?', self.id)
    return unseen_comments
  end

  def clear_unseen_comments
    u_c = unseen_comments
    u_c.update(:seen=>true) if not u_c.empty?
  end
  
  def outstanding_points
    outstanding_points = Point
      .where('seen=?', false)
      .joins(request_group: :participation)
      .where('participations.user_id=?', self.id)

    return outstanding_points
  end

  def outstanding_points_value
    return outstanding_points.sum(:value)
  end

  def clear_outstanding_points
    return outstanding_points.update_all(:seen=>true)
  end

  def to_hash
    {
      email: self.email,
      first_name: self.first_name,
      last_name: self.last_name,
      id: self.id,
      is_guest: self.is_guest,
      profile_picture_url: profile_picture_url
    }
  end
  
  def unseen_notifications
   notifications = FaceoffNotification.find_by_sql(
     ["SELECT faceoff_notifications.* FROM faceoff_notifications 
          INNER JOIN requests 
          ON requests.id = faceoff_notifications.request_id 
          WHERE ((requests.user_id = ?)
            AND (seen = ?)
            AND (requests.photo_id IS NULL))
        
        UNION

        SELECT faceoff_notifications.* FROM faceoff_notifications 
          INNER JOIN participations 
          ON participations.id = faceoff_notifications.participation_id 
          INNER JOIN requests
          ON faceoff_notifications.request_id = requests.id
          WHERE ((participations.user_id = ?)
            AND (seen = ?)
            AND (requests.photo_id IS NOT NULL))",
        self.id, false, self.id, false])

    notifications
  end

  def should_notify_notifications 
   notifications = FaceoffNotification.find_by_sql(
     ["SELECT faceoff_notifications.* FROM faceoff_notifications 
          INNER JOIN requests 
          ON requests.id = faceoff_notifications.request_id 
          WHERE ((requests.user_id = ?)
            AND (should_notify = ?)
            AND (requests.photo_id IS NULL))
        
        UNION

        SELECT faceoff_notifications.* FROM faceoff_notifications 
          INNER JOIN participations 
          ON participations.id = faceoff_notifications.participation_id 
          INNER JOIN requests
          ON faceoff_notifications.request_id = requests.id
          WHERE ((participations.user_id = ?)
            AND (should_notify = ?)
            AND (requests.photo_id IS NOT NULL))
          ",
        self.id, true, self.id, true])

    notifications
  end

  def saw_unseen_notifications
    sql = "
      UPDATE faceoff_notifications
      SET seen='t'
      WHERE faceoff_notifications.id
      IN
      (SELECT faceoff_notifications.id FROM faceoff_notifications 
        INNER JOIN requests 
        ON requests.id = faceoff_notifications.request_id 
        WHERE ((requests.user_id = #{self.id}) AND (seen = 'f'))
        
      UNION

      SELECT faceoff_notifications.id FROM faceoff_notifications 
        INNER JOIN participations 
        ON participations.id = faceoff_notifications.participation_id 
        WHERE ((participations.user_id = #{self.id}) AND (seen = 'f')))
      "
      
    ActiveRecord::Base.connection.execute(sql)
  end

  def saw_should_notify_notifications
    sql = "
      UPDATE faceoff_notifications
      SET should_notify='f'
      WHERE faceoff_notifications.id
      IN
      (SELECT faceoff_notifications.id FROM faceoff_notifications 
        INNER JOIN requests 
        ON requests.id = faceoff_notifications.request_id 
        WHERE ((requests.user_id = #{self.id})
        AND (should_notify = 't')
        AND (requests.photo_id IS NULL))
        
      UNION

      SELECT faceoff_notifications.id FROM faceoff_notifications 
        INNER JOIN participations 
        ON participations.id = faceoff_notifications.participation_id 
        INNER JOIN requests
        ON faceoff_notifications.request_id = requests.id
        WHERE ((participations.user_id = #{self.id})
        AND (should_notify = 't')
        AND (requests.photo_id IS NOT NULL)))
      "
      
    ActiveRecord::Base.connection.execute(sql)
  end
end
