class IndexFaceoffSerializer < ApplicationSerializer 
  attributes :id, :updated_at, :name
  has_many :users

  def faceoff_id
    object.id
  end

  def updated_at
    object.updated_at.to_i
  end
end
