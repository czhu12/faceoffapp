class TopicSerializer < ApplicationSerializer
  attributes :name, :count
  cached
  delegate :cache_key, to: :object
end
