class UserSerializer < ApplicationSerializer
  attributes :id, :email, :first_name, :last_name, :profile_picture_url, :is_facebook_user, :fbid, :phone_number, :is_guest

  cached
  delegate :cache_key, to: :object

  def is_facebook_user
    not object.fbid.nil?
  end

  def profile_picture_url
    object.profile_picture_url
  end

  def phone_number
    if object.device.nil?
      return nil
    end

    object.device.phone_number
  end
end
