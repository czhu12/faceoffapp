class FaceoffSerializer < ApplicationSerializer
  attributes :id, :request_bundles, :name
  has_many :users

  def request_bundles
    object.request_groups
    .includes(
      :photos, 
      :topic, 
      :participation, 
      :users, 
      :sender, 
      :requests
    )
    .order('updated_at DESC').map do |request_group| 
      responses = []
      pending_requests = []

      request_group.requests.each do |request|

        if request.photo
          responses.push({
            request_id: request.id,
            receiver: get_user_with_photo(request.user),
            photo: {
              id: request.photo.id,
              photo_url: request.photo.face.url(:original),
            }
          })
        else

          pending_requests.push({
            request_id: request.id,
            receiver: get_user_with_photo(request.user),
            photo: {
              photo_url: '/images/medium/missing.png',
            }
          })

        end

      end

      { responses: responses.sort_by { |k| k['updated_at'] }.reverse,
        pending_requests: pending_requests.sort_by { |k| k['updated_at'] }.reverse,
        id: request_group.id,
        topic: request_group.topic,
        sender: request_group.sender.to_hash }
      end
  end

  def users
    object.users
  end

  def json_for(target, options = {})
    options[:scope] ||= self
    options[:url_options] ||= url_options
    target.active_model_serializer.new(target, options).to_json
  end

  #TODO This method actually sucks because it redefines what it means
  # to serialize a user for faceoffs only. #XtremeProgramming
  def get_user_with_photo(req_user)
    { 
      is_facebook_user: req_user.is_facebook_user,
      id: req_user.id,
      first_name: req_user.first_name,
      last_name: req_user.last_name,
      profile_picture_url: req_user.profile_picture_url,
    }
  end
end
