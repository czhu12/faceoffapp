class RequestSerializer < ApplicationSerializer
  attributes :id, :photo_url, :user_id
  
  def photo_url
    return nil unless object.photo

    "http://#{FaceoffApp::Application::HOST_IP}#{object.photo.face.url(:original)}"
  end
end
