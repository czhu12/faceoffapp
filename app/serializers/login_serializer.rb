class LoginSerializer < ApplicationSerializer
  attributes :id, :email, :first_name, :last_name, :access_token, :fbid, :phone_number

  def phone_number
    if object.device.nil?
      return nil
    end

    object.device.phone_number
  end
end
