class FaceoffNotificationSerializer < ActiveModel::Serializer
  attributes :id, :faceoff_id, :request_id, :type, :requester, :photo_url, :topic, :responder, :should_notify, :seen

  def faceoff_id
    object.faceoff.id
  end

  def type
    object.type
  end

  def topic
    return nil if not @options[:singular]
    
    object.request.topic
  end

  def requester
    return nil if not @options[:singular]

    if object.request.requester.nil?
      return nil
    end

    return object.request.requester.to_hash
  end

  def responder
    return nil if not @options[:singular]

    if object.request.responder.nil?
      return nil
    end

    return object.request.responder.to_hash
  end

  def photo_url
    return nil if not @options[:singular]

    if object.request.photo_id.nil? || object.request.photo.face.nil?
      return nil
    end

    return "#{FaceoffApp::Application::HOST_IP}" + object.request.photo.face.url
  end
end
