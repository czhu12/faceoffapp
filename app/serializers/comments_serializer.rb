class CommentsSerializer < ActiveModel::Serializer
  attributes :id, :body, :user_id, :photo_id
  has_one :user
  has_one :topic
end
