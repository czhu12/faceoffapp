require 'facebook_lib'

class StatisticsSerializer < ActiveModel::Serializer
  attributes :profile_picture_url, :responses_count, :requests_count

  def profile_picture_url
    object.profile_picture_url
  end

  def responses_count
    object.responses_count
  end

  def requests_count
    object.requests_count
  end
end
