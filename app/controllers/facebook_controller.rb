require 'facebook_lib'

class FacebookController < ApplicationController
  before_filter :authenticate

  def friends
    if current_user.is_facebook_user
      render :json => current_user.facebook_friends
    elsif params[:fbid] and params[:fb_access_token]
      Rails.logger.debug("fbid #{params[:fbid]} fb_access_token #{params[:fb_access_token]}")
      render :json => current_user.facebook_friends(params[:fbid], params[:fb_access_token])
    else
      render :json => {error: 'Not a valid facebook user'}, status: 401
    end
  end

end
