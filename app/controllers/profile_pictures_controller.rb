class ProfilePicturesController < ApplicationController
  def create

    profile_picture = ProfilePicture.new
    profile_picture.photo = params[:photo]
    profile_picture.save

    if profile_picture.save
      current_user.profile_picture = profile_picture
      render :json => profile_picture
    else 
      render :json => {:error => profile_picture.errors}
    end
  end
end
