class DevicesController < ApplicationController
  before_filter :authenticate
  def create
    device = Device.find_by_phone_number(params[:phone_number])

    if device
      # A user with this device is found, we should return the user of this device
      render :json => device.user
      return
    end

    device = Device.new(params[:device])

    user = User.new(
      :first_name => params[:first_name], 
      :last_name => params[:last_name],
      :email=>"guest_#{Time.new.to_i}#{rand(99)}@guest.com"
    )

    user.is_guest = true
    device.user = user

    Device.transaction do
      user.save!(:validate => false)
      if device.save!
        render json: user
      else
        render json: {:errors => user.errors.concat(device.errors)}
      end
    end
  end

  def update
    device = current_user.device
    if device.nil?
      device = Device.new
      device.user_id = current_user.id
    end

    if device.update_attributes(:phone_number => params[:device][:phone_number])
      render json: device
    else
      render :json => {field: "phone_number", errors: ["Update Failed"]}, status: 422
    end
  end
end
