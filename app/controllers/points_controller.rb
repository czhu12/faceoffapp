require 'points/points_notifier'

class PointsController < ApplicationController

  def index
    total_points = current_user.outstanding_points_value
    outstanding_points = current_user.outstanding_points.includes(:topic)
    #current_user.clear_outstanding_points
    render :json => {total_points: total_points}
  end

  def create_bulk
    send_notification
    render :json => {message: "Success"}
  end

  def send_notification
    bulk_points = params[:bulk_points]
    bulk_points.each do |point|
      value = point[:value]
      request_group_id = point[:request_group_id]

      Rails.logger.debug("Value: #{value}, RequestGroupID: #{request_group_id}")
      PointsNotifier.perform_async(
        value,
        current_user.id,
        request_group_id
      )
    end
  end
end
