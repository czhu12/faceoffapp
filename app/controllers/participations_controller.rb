class ParticipationsController < ApplicationController
  before_filter :authenticate

  def create
    participation = Participation.new
    participation.faceoff_id = params[:participation][:faceoff_id]
    participation.user_id = params[:participation][:user_id]
    if participation.save
      render :json => {message: "Success"}
    else
      render :json => {errors: participation.errors }
    end
  end
end
