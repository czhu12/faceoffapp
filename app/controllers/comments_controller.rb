require 'comments/comments_notifier'

class CommentsController < ApplicationController
  def create
    @comment = Comment.new(params[:comment])
    @comment.photo_id = params[:id]
    @comment.user = current_user
    if @comment.save
      render :json => @comment, serializer: CommentsSerializer
      send_notification
    else 
      Rails.logger.debug @comment.errors.inspect
      render :json => {:error=>@comment.errors}, status: 406
    end
  end

  def unseen_comments
    render :json => current_user.unseen_comments.includes(:user), each_serializer: CommentsSerializer
  end

  def index
    @photo = Photo.find(params[:id])
    render :json => @photo.comments, each_serializer: CommentsSerializer
  end

  private
  def send_notification
    # get the comment id and then send it.
    CommentsNotifier.perform_async(@comment.user_id, @comment.body)
  end
end
