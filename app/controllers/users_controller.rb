require 'facebook/new_facebook_friend_notifier'

class UsersController < ApplicationController
  before_filter :authenticate, :only=>[:update, :index]
  #TODO require login
  def index
    # This should find all friends of a user given a set of phone_numbers
    # Caching policy: {current_user.id}{User.maximum(:updated_at)}{User.count}
    render json: User.includes(:profile_picture, :device).all.select { |user| user.id != current_user.id}, :root => false
  end 

  def update
    if current_user.authenticate(params[:user][:password])
      if params[:password_reset]
        current_user.password = params[:password_reset]
        current_user.password_confirmation = params[:password_reset]
      end

      if current_user.save
        render json: current_user
      else
        puts current_user.errors
        render :json => {:message => "Error updating user"}, :status => 403
      end
    else
      render :json => {:message => "Unauthorized"}, :status => :unauthorized
    end
  end

  def contacts
    phone_numbers = params[:phone_numbers]

    if phone_numbers.nil?
      render json: []
    end

    # Format phone numbers
    phone_numbers = phone_numbers.map do |phone_number|
      phone_number.scan(/\d+/).join
    end

    devices = Device.includes(:user).select do |device|
      phone_numbers.include? device.phone_number
    end

    registered_users = devices
      .select { |device|  device.user }
      .map { |device| 
        user = device.user
        user
      }
      
    render json: registered_users, each_serializer: UserSerializer
  end

  def create
    logger.debug(params.inspect)
    if params[:user][:phone_number] and check_for_user_and_update
      return
    end

    user = User.new(pick(params[:user], :email, :password, :password_confirmation, :first_name, :last_name))
    if user.save
      render json: user, serializer: LoginSerializer
      create_device_for_user(user)
    else
      user.errors.each do |error|
        puts error
      end
      
      # Here we should return a variety of error codes depending on what was made.
      puts format_error_messages (user.errors.messages)
      render :json => format_error_messages(user.errors.messages), status: 401
    end
  end

  def create_device_for_user(user)
    if params[:user][:phone_number]
      device = Device.new(:phone_number => params[:user][:phone_number])
      device.user = user
      device.save
    end
  end

  def create_guest
    phone_number = params[:user][:phone_number]

    if phone_number
      user = User.find_by_phone_number(phone_number)

      if user.nil?
        user.email = gen_guest_email
        user.password = user.password_confirmation = gen_guest_password
        user.save
        create_device_for_user(user)
      end 

      if user.errors
        render json: { errors: user.errors }
      end

      render json: user
    else
      render json: { error: "No phone number specified" }
    end
  end

  #def update
  #  user = User.find(params[:id])
  #  if user.update_attributes(params[:user])
  #    render :json => {:access_token => user.access_token}
  #  else
  #    render :json => {:error => user.errors}, status: 422
  #  end
  #end

  def login
    user = User.find_by_email(params[:user][:email])
    if user && user.authenticate(params[:user][:password])
      render json: user, serializer: LoginSerializer
    else
      if user.nil?
        render :json => {field: "email", errors: ["Email not found."]}, status: 422
      else
        render :json => {field: "email", errors: ["Authentication failed."]}, status: 422
      end
    end
  end

  def facebook
    user = User.find_by_fbid(params[:user][:fbid])
    
    if user.nil?
      name_array = params[:name].split(' ')
      first_name = name_array[0]
      last_name = name_array[-1]

      if first_name == last_name
        last_name = ''
      end

      user = User.new(params[:user].merge({first_name: first_name, last_name: last_name}))
      user.save(validate: false)
      render :json => user, serializer: LoginSerializer
      notify_friends(user)
    else 
      user.fb_access_token = params[:user][:fb_access_token]
      user.save
      render :json => user, serializer: LoginSerializer
    end
  end

  def user
    render :json => current_user, serializer: CurrentUserSerializer
  end

  private
  def check_for_user_and_update
    device = Device.find_by_phone_number(params[:user][:phone_number])
    if device && device.user && device.user.is_guest
      # We have found a user associated with this device.
      user = device.user
      user.is_guest = false
      # Don't know why this phone number is included in params[:user]
      if user.update_attributes(filter params[:user], :phone_number)
        render json: user
        return true
      end
    end
    return false
  end

  def gen_guest_email
    "#{SecureRandom.hex(32)}@faceoffapp.com"
  end

  def gen_guest_password
    "#{SecureRandom.hex(16)}"
  end

  def notify_friends(new_facebook_user)
    NewFacebookFriendNotifier.perform_async(new_facebook_user.id)
  end
end
