require 'notifications/notification_sender'

class FaceoffsController < ApplicationController
  before_filter :authenticate
  before_filter :find_faceoff, :only => :show
  before_filter :clear_comments, :only=>:show
  before_filter :clear_should_notify_notifications, :only => [:show, :index]
  # This endpoint returns the most recent 3 photo 
  # urls, as well as all outstanding requests and 
  # faces.
  def index
    faceoffs = current_user.faceoffs.includes(
      :devices,
      :profile_pictures,
      :requests, 
      :users, 
      :participations
    )

    sorted_faceoffs = get_sorted_faceoffs(faceoffs)

    render json: sorted_faceoffs, each_serializer: IndexFaceoffSerializer, current_user: current_user
  end

  # TODO: We need to handle errors here. Likely 
  # have to look at some kind of transactions for 
  # doing so.
  def create
    # Find if topic exists already
    topic_name = params[:topic_name]
    topic = Topic.find_by_name(topic_name)

    if not topic
      topic = Topic.new
      topic.name = topic_name
      topic.save
    end

    user_ids = params[:user_ids]
    if not user_ids.nil?
      render :json => {error: "Too many users" }, status: 403 if user_ids.length > 6
    end
    # Have to find out if faceoff exists between
    # these two users already.
    #faceoff = current_user.find_faceoff_with(user_ids)
    user_ids = user_ids.push(current_user.id)
    faceoff = Faceoff.faceoff_with_users(user_ids)

    # If the faceoff doesn't exist we have to create it.
    if not faceoff
      faceoff = Faceoff.new
      faceoff.save

      # Wire up participations for all users.
      participations_to_create = []
      Participation.create(user_id: current_user.id, faceoff_id: faceoff.id)
      user_ids.each do |user_id|
        participation = Participation.new(user_id: user_id, faceoff_id: faceoff.id)
        participations_to_create.push(participation)
      end

      # Doesn't call callbacks.
      Participation.import participations_to_create
    end

    faceoff.touch
    authorize! :update, faceoff

    request_group = RequestGroup.includes(:topic, :participation).new
    request_group.topic = topic
    request_group.participation = current_user.participation_for_faceoff(faceoff.id)
    request_group.save

    requests_to_create = []
    # THIS IS CHRIS' TRANSACTION SO DONT EVEN TOUCH IT YOU MANIAC.
    Request.transaction do
      # Yes, this should be 0
      maximum_request_id = Request.maximum(:id) || 0

      user_ids.each do |user_id|
          maximum_request_id += 1

          request = Request.new
          request.id = maximum_request_id
          request.user_id = user_id
          request.request_group_id = request_group.id

          requests_to_create.push(request)
      end

      result = Request.import requests_to_create, :validate => true
    end

    send_notifications(requests_to_create)

    render json: {id: faceoff.id, request_group: request_group.id}
  end

  # This endpoint returns all info about a certain
  # faceoff, it returns all photo urls, in this
  # faceoffs history, all requests, and all 
  # participating users
  def show
    if @faceoff
      authorize! :read, @faceoff
      render json: @faceoff, current_user: current_user, page: params[:page]
      @faceoff.clear_notifications_for_user(current_user.id)
    else
      render json: { errors: ["Not found"] }
    end
  end

  def update
    faceoff = Faceoff.find(params[:id])
    faceoff.name = params[:name]
    if faceoff.save
      render :json => {message: "Success"}
    else
      render :json => {error: faceoff.errors}
    end
  end

  def get_sorted_faceoffs(faceoffs)
    faceoffs_sorted = faceoffs.sort do |f1, f2|
      # return 1 if f1 more recent than f2
      # return 0 if equal
      # return -1 if f2 more recent than f1
      f2.updated_at <=> f1.updated_at
    end

    return faceoffs_sorted
  end

  private 
  def send_notifications(requests)
    requests.each do |request|
      next if request.user_id == request.request_group.participation.user_id
      Rails.logger.debug "User: #{current_user.id} is sending a request to #{request.user_id}, with request id: #{request.id}"
      
      NotificationSender.perform_async(
        request.id, 
        request.request_group.participation_id
      )
    end
  end

  def clear_comments
    @faceoff.clear_comments_for(current_user)
  end

  def find_faceoff
    @faceoff = Faceoff.eager_load(:requests).find(params[:faceoff_id])
  end

  def clear_should_notify_notifications
    current_user ? current_user.saw_should_notify_notifications : nil
  end

end
