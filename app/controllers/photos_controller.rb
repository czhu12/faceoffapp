require 'notifications/notification_sender'

class PhotosController < ApplicationController
  before_filter :authenticate
  after_filter :clear_cache

  def create
    request = Request.find(params[:request_id])
    @photo = Photo.new
    @photo.face = params[:face]
    @photo.request = request

    if @photo.save
      puts "==============="
      puts @photo.face.url
      puts "==============="
      render :json => {:message => @photo.face}
    else
      render :json => {:errors => @photo.errors}
    end

    
  end

  def points
    bulk_points = params[:bulk_points]
    if bulk_points.nil?
      render :json => {:message => "Success"}
      return
    end

    bulk_points.each do |photo_points|
      photo_id = photo_points[:photo_id]
      points = photo_points[:points]
      photo = Photo.find(photo_id)
      photo.give_points(points)
    end
    
    render :json => {:message => "Success"}
  end

  private
  def clear_cache
    @photo.request.request_group.faceoff.touch
  end
end
