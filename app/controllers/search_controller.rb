class SearchController < ApplicationController
  def index
    type = params[:type]
    query = params[:query]
    if type == 'users'
      results = User.where('email LIKE ? OR first_name LIKE ? OR last_name LIKE ?', "%#{query}%", "%#{query}%", "%#{query}%");
      if results.count > 100
        render json: {message: 'Too many results'}
      else 
        render json: results, each_serializer: UserSerializer
      end
      return
    elsif type == 'something else'
    end

    render json: {message: 'Must provide a type'}
  end
end
