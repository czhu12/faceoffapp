class RequestsController < ApplicationController
  before_filter :authenticate

  # serializer can not handle an index function. Don't make one.

  def show
    request = Request.find params[:id]
    render json: request
  end

  def create
    topic_name = params[:topic_name]
    topic = Topic.find_by_name(topic_name)

    if not topic
      topic = Topic.new
      topic.name = topic_name
      topic.save
    end

    @faceoff = Faceoff.find(params[:faceoff_id])
    authorize! :update, @faceoff

    request_group = RequestGroup.includes(:topic, :participation).new
    request_group.topic = topic
    request_group.participation = current_user.participation_for_faceoff(@faceoff.id)
    request_group.save
    
    user_ids = params[:user_ids]
    requests_to_create = []
    Request.transaction do
      # Yes, this should be 0
      maximum_request_id = Request.maximum(:id) || 0
      user_ids.each do |user_id|
        maximum_request_id += 1

        request = Request.new
        request.id = maximum_request_id
        request.user_id = user_id
        request.request_group_id = request_group.id

        requests_to_create.push(request)
      end

      result = Request.import requests_to_create, :validate => true
    end

    send_notifications(requests_to_create)

    clear_cache
    #TODO Must check for errors here
    render json: {id: @faceoff.id, request_group: request_group.id}
  end

  private

  def send_notifications(requests)
    requests.each do |request|
      next if request.user_id == request.request_group.participation.user_id
      Rails.logger.debug "User: #{current_user.id} is sending a request to #{request.user_id}, with request id: #{request.id}"
      
      NotificationSender.perform_async(
        request.id, 
        request.request_group.participation_id
      )
    end
  end

  def clear_cache 
    @faceoff.touch
  end
end
