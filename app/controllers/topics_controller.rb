class TopicsController < ApplicationController
  def index
    # We can cache this.
    # Caching Policy:
    # evict cache if any topic is changed.
    query = params[:filter]
    if query == 'trending'
      topics = Rails.cache.fetch("#{Topic.maximum(:updated_at).try(:to_i)}#{Topic.count}-trending", :expires_in => 5.minutes) do
        Topic.order('count DESC').limit(100)
      end
    elsif query == 'recent'
      topics = Rails.cache.fetch("#{Topic.maximum(:updated_at).try(:to_i)}#{Topic.count}-recent", :expires_in => 5.minutes) do
        Topic.order('updated_at DESC').limit(100)
      end
    else 
      topics = Rails.cache.fetch("#{Topic.maximum(:updated_at).try(:to_i)}#{Topic.count}-trending", :expires_in => 5.minutes) do
        Topic.order('count DESC').limit(100)
      end
    end

    render json: topics, :root=>false
  end

  def create
    topic = Topic.find_by_name(params[:topic][:name]) 

    if not topic.nil?
      topic.inc
      render json: topic
    else
      topic = Topic.new(params[:topic])
      if topic.save
        render json: topic
      else
        render json: { error: topic.errors }
      end
    end
  end
end
