class FaceoffNotificationsController < ApplicationController
  before_filter :authenticate
  skip_before_filter :clear_should_notify_notifications
  # This should really be kept in a cache

  # There will be two different 'all' notifications:
  # type=unseen: Should return all notifications 
  # that should be shown in the main page.
  #
  # type=should_notify: Should return all notifications
  # that should be shown to the user when they receive 
  # a push notification.
  def index_unseen_notifications
    notifications = current_user.unseen_notifications

    if notifications.count == 1
      render json: notifications, singular: true
    else
      render json: notifications, singular: false
    end
  end

  def index_should_notify
    notifications = current_user.should_notify_notifications

    if notifications.count == 1
      render json: notifications, singular: true
    else
      render json: notifications, singular: false
    end
  end

  # A destroy should be able to destroy two different things:
  # All the notifications of a faceoff
  # All the should_notify notifications
  def destroy_unseen_notifications
    faceoff = Faceoff.find(params[:faceoff_id])
    faceoff.clear_notifications_for_user(current_user.id)

    render json: {message: "Success"}
  end

  def destroy_should_notify
    current_user.saw_notifications

    render json: {message: "Success"}
  end

  def show_unseen_notifications
    faceoff = Faceoff.find params[:faceoff_id]
    notifications = faceoff.notifications_for current_user.id

    render json: faceoff.notifications
  end
end
