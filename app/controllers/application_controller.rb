require 'authentication'
require 'perf/development_profiler'

class ApplicationController < ActionController::Base
  protect_from_forgery
  helper :all
  include Authentication

  
  rescue_from CanCan::AccessDenied do |exception|
    render :json => {:message => "Forbidden"}, :status => :forbidden
  end
  
  def pick(hash, *keys)
    filtered = {}
    hash.each do |key, value| 
      filtered[key.to_sym] = value if keys.include?(key.to_sym) 
    end
    filtered
  end

  def filter(hash, *keys)
    filtered = {}
    hash.each do |key, value| 
      filtered[key.to_sym] = value if not keys.include?(key.to_sym) 
    end
    filtered
  end

  def authenticate
    if current_user.nil?
      render :json => {:message => "Not logged in."}, :status => :unauthorized
    end
  end

  def default_serializer_options
    {root: false}
  end

  def format_error_messages(messages_hash)
    #{:password => "didn't match", :email=>'invalid'}
    # Here we should 
    
    error_objs = []
    messages_hash.each do |field, errors|
      error_objs.push({field: field.to_s, errors: errors})
    end

    error_objs
  end
end
