class CrashReportsController < ApplicationController
  def create
    report = CrashReport.create(params[:crash_report])
    render :json => report 
  end
end
