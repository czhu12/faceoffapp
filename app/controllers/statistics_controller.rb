class StatisticsController < ApplicationController
  before_filter :authenticate
  def index
    render :json => current_user, serializer: StatisticsSerializer, :root => false
  end
end
